package com.afs.unittest;

import com.afs.unittest.exception.UnexpectedProjectTypeException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ExpenseServiceTest {
    @Test
    void should_return_internal_expense_type_when_getExpenseCodeByProject_given_internal_project() {
        // given
        Project internalProject = new Project(ProjectType.INTERNAL, "project name");
        ProjectClient projectClient = new ProjectClient();
        ExpenseService expenseService = new ExpenseService(projectClient);

        // when
        ExpenseType actual = expenseService.getExpenseCodeByProject(internalProject);

        // then
        assertEquals(ExpenseType.INTERNAL_PROJECT_EXPENSE, actual);
    }

    @Test
    void should_return_expense_type_A_when_getExpenseCodeByProject_given_project_is_external_and_name_is_project_A() {
        // given

        // when

        // then
    }

    @Test
    void should_return_expense_type_B_when_getExpenseCodeByProject_given_project_is_external_and_name_is_project_B() {
        // given

        // when

        // then
    }

    @Test
    void should_return_other_expense_type_when_getExpenseCodeByProject_given_project_is_external_and_has_other_name() {
        // given

        // when

        // then
    }

    //optional
    @Test
    void should_throw_unexpected_project_exception_when_getExpenseCodeByProject_given_project_is_invalid() {
        // given

        // when

        // then
    }

    /*
    @Test
    void should_return_submit_state_when_submit_expense_given_project_do_not_expired() {
        // given

        // when

        // then
    }

    @Test
    void should_throw_expired_exception_when_submit_expense_given_project_has_expired() {
        // given

        // when

        // then
    }*/
}
