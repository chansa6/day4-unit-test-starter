package com.afs.unittest;

public class ProjectClient implements IProjectClient{
    @Override
    public boolean isProjectTypeValid(ProjectType projectType) {
        // some pseudo work: connect db ...
        // some pseudo work: call another http service ...
        return projectType == ProjectType.EXTERNAL || projectType == ProjectType.INTERNAL;
    }

    @Override
    public boolean isExpired(Project project) {
        throw new UnsupportedOperationException("this will be implemented by group C");
    }
}
